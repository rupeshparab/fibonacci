import java.util.Scanner;

public class Fib3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if(n<1 || n>10)
            throw new IllegalArgumentException("Must be in the range 1..10"); 
//        long startTime = System.nanoTime();
        
        for(int i = 0; i < n; i++)
        {
            System.out.println(fib(i));
        }
        
//        long stopTime = System.nanoTime();
//        System.out.println((stopTime - startTime) / 1000000000.0);
    }
    
    private static int fib(int n)
    {
        if(n==0 || n==1)
            return n;
        if((1&n) == 1)
        {
            int k = (n+1)/2;
            return (int)(Math.pow(fib(k), 2) + Math.pow(fib(k-1), 2));
        }
        else
        {
            int k = n/2;
            int temp = fib(k);
            return temp*(temp + 2*fib(k-1));
        }
    }
}

/*
n = 10 => 3.62641E-4  sec
n = 45 => 10.57537E-4 sec
*/
