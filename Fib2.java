import java.util.Scanner;

public class Fib2 {
    
    static int series[] = new int[45];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if(n<1 || n>10)
            throw new IllegalArgumentException("Must be in the range 1..10");

//        long startTime = System.nanoTime();
        
        for(int i = 0; i < n; i++)
        {
            System.out.println(fib(i));
        }
        
//        long stopTime = System.nanoTime();
//        System.out.println((stopTime - startTime) / 1000000000.0);
    }
    
    private static int fib(int i)
    {
        if(i==0 || i==1)
        {
            series[i]=i;
            return i;
        }
        if(series[i]==0)
            series[i] = fib(i-1) + fib(i-2);
        return series[i];
    }
}

/*
n = 10 => 3.88063E-4 sec
n = 45 => 9.07065E-4 sec
*/
