# README #

## Fibonacci ##

This repository is my solution to the following problem statement:


```
#!shell

There is a function fibo(N) such that

fibo(n) = N, N <= 1

= fib(N-2) + fib(N-1), otherwise

Write a program that implements fibo(N), taking N as input and giving the output as mentioned

below.

Constraints:

1 ≤ N ≤ 10

Sample Input

4

Sample Output

0

1

1

2
```


## Note ##

I have handled the input according to the constraints mentioned in the problem statement.
Although to compare performance of the 3 approaches I have used a higher valued input. 

## 3 Approaches to Fibonacci ##

### Basic Recursion ###
It uses recursion and no result is reused.

### Dynamic Programming ###
Results are stored and reused to generate new results. Memory is required to store results.

### Dijkstra's Method ###
Highly optimized method which uses a mathematical formula by Dijkstra. Performance is nearly equivalent to Dynamic Approach but memory requirement is low.

## Comparison of 3 Approaches ##

Below is the comparison of complexities is to find the nᵗʰ number in the Fibonacci Series

| Approach              | Time Complexity  | Space Complexity  |
| --------------------- |:----------------:| -----------------:|
| Basic Recursion       | O(2ⁿ)            | O(1)              |
| Dynamic Programming   | O(n)             | O(n)              |
| Dijkstra's Method     | O(log₂n)         | O(1)              |

Below is comparison of time(sec) to generate first n numbers in the Fibonacci series

| Approach              | n = 10           | n = 45            |
| --------------------- |:----------------:| -----------------:|
| Basic Recursion       | 4.57368 x 10⁻⁴   | 8.72614           |
| Dynamic Programming   | 3.88063 x 10⁻⁴   | 9.07065 x 10⁻⁴    |
| Dijkstra's Method     | 3.62641 x 10⁻⁴   | 10.57537 x 10⁻⁴   |