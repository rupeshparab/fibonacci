import java.util.Scanner;

public class Fib1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if(n<1 || n>10)
            throw new IllegalArgumentException("Must be in the range 1..10"); 

//        long startTime = System.nanoTime();
        for(int i = 0; i < n; i++)
        {
            System.out.println(fib(i));
        }
        
//        long stopTime = System.nanoTime();
//        System.out.println((stopTime - startTime) / 1000000000.0);
    }
    
    private static int fib(int i)
    {
        if(i==0 || i==1)
            return i;
        else
            return fib(i-1) + fib(i-2);
    }
}

/*
n = 10 => 4.57368E-4  sec
n = 45 => 8.726141762 sec
*/
